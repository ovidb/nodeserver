const jwt = require('jwt-simple');
const UserModel = require('../models/user');
const config = require('../config');

const tokenForUser = (user) => {
  const timestamp = new Date().getTime()
  return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
};

exports.signin = (req, res, next) => {
  // user has already had their email and password auth'd
  // we need to return a token
  res.send({ token: tokenForUser(req.user) });
  next();
};

exports.signup = (req, res, next) => {
  // Parse body
  const email = req.body.email;
  const password = req.body.password;

  if(!email || !password) { // ensure u & p are provided
    return res.status(422).send({ error: 'You must proivde email and password' });
  }

  // See if user with email exists
  UserModel.findOne({ email: email }, (err, existingUser) => {
    if (err) {
      return next(err);
    }
    // If exists return error
    if (existingUser) {
      return res.status(422).send({ error: 'Email is in use'});
    }
    // create user email
    const user = new UserModel({
      email: email,
      password: password,
    })

    user.save((err) => {
      if (err) { return next(err) }

      // respond w/ success
      res.json({ token: tokenForUser(user) });
    });

  });
};

