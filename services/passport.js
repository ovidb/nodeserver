const passport = require('passport');
const UserModel = require('../models/user');
const config = require('../config');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');


// Create local strategy
const localOptions = { usernameField: 'email' };
const localLogin = new LocalStrategy(localOptions, (email, password, done) => {
  // verify email and pass
  UserModel.findOne({ email: email }, (err, user) => {
    if(err) { return done(err); }
    if(!user) { return done(null, false) } // user not found

    // compare password
    user.comparePassword(password, (err, isMatch) => {
        if(err) { done(err); }
        if(!isMatch) { return done(null, false); }

        return done(null, user);
    })

  });

});


// Setup strategy
const jwtOption = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey : config.secret,
};


// Create strategy
const jwtLogin = new JwtStrategy(jwtOption, function(payload, done) {
  // Check to see if the user id from the payload matches
  // any of the users in our database
  // If it matches return done with that
  // Else return done with false
  UserModel.findById(payload.sub, (err, user) => {
    if (err) { return done(err, false)}

    if (user) {
      return done(null, user);
    } else {
      return done(null, false);
    }
  })
});

// Tell passport to use this strategy.
passport.use(localLogin);
passport.use(jwtLogin);
