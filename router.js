const authentication = require('./controllers/authentication');
const passportService = require('./services/passport');
const passport = require('passport');

const requrieAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', {session: false});

module.exports = (app) => {
  app.get('/', requrieAuth, (req, res) => {
    res.send({ message: 'Hi there' });
  });
  // we use a middleware to verify email/pass before hitting the singin handler
  app.post('/signin', requireSignin, authentication.signin);
  app.post('/signup', authentication.signup);
}