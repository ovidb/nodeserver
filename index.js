// App starting point
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');

const app = express();
const router = require('./router');
const mongoose = require('mongoose');

// DB Setup
mongoose.connect('mongodb://localhost:auth/auth'); // auth/auth is the database name that will be created

// # App Setup

//use cors
app.use(cors());
// morgan is logging all request to console
app.use(morgan('combined'));
// treat every request as json
app.use(bodyParser.json({ type: '*/*' }));
router(app);

// # Server Setup
const port = process.env.PORT || 9090;
const server = http.createServer(app);
server.listen(port);
console.log(`Server listening on: ${port}`);