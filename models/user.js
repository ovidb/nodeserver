const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');


// Define the model
const userSchema = new Schema({
  email: { type: String, unique: true, lowercase: true },
  password: String,
});

// On SaveHook, encrypt pass
userSchema.pre('save',function(next) {
  const user = this;
  // generate a salt
  bcrypt.genSalt(10, (err,salt) => {
    if (err) { return next(err); }
    // with that salt generate a password
    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) { return next(err); }

      user.password = hash;
      next();
    })
  })
});

// compare password user method
userSchema.methods.comparePassword = function(canditatePassword, callback) {
  // use bcrypt to compare the canditate password and stored password
  bcrypt.compare(canditatePassword, this.password, (err, isMatch) => {
    if (err) { return callback(err); }
    // return the comparison
    callback(null, isMatch);
  })
};

// Create the model
const UserModel = mongoose.model('user', userSchema);

// Export model
module.exports = UserModel;